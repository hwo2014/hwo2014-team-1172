{-# LANGUAGE OverloadedStrings #-}

module ServerMessage where

import Data.Aeson(FromJSON(..), parseJSON, (.:), Value(..))
import qualified Data.HashMap.Lazy as H
import Control.Applicative ((<$>))

import GameInitModel
import CarPositionsModel

data Message = Join | GameInit GameInitData | CarPositions [CarPosition] | Unknown String

instance FromJSON Message where
  parseJSON (Object v) = do
    case H.lookup "msgType" v of
      Just "join" -> return Join
      Just "gameInit" -> GameInit <$> (v .: "data")
      Just "carPositions" -> CarPositions <$> (v .: "data")
      Just unknown -> return $ Unknown (show unknown)
      Nothing -> fail "No message type"
