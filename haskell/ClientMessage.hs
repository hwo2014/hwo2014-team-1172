{-# LANGUAGE OverloadedStrings #-}

module ClientMessage where

import Data.Aeson(ToJSON(..), toJSON, (.=), object, Value(Null))

data Message = Join { jBotname :: String, jBotkey :: String }
             | Throttle Double
             | Ping

instance ToJSON Message where
  toJSON m = case m of
    (Join botname botkey) -> msg "join" $ object [ "name" .= botname
                                                 , "key"  .= botkey ]
    (Throttle amount) -> msg "throttle" (show amount)
    (Ping) -> msg "ping" Null
   where
    msg :: (ToJSON a) => String -> a -> Value
    msg typ dat = object [ "msgtype" .= typ
                         , "data"    .= dat ]
