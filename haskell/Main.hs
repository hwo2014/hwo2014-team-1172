{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(encode, decode, fromJSON, Result(..))

import GameInitModel
import CarPositionsModel

import qualified ServerMessage as Server
import qualified ClientMessage as Client

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ L.unpack $ encode $ Client.Join botname botkey
  handleMessages h

handleMessages h = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      case fromJSON json of
        Success serverMessage -> handleServerMessage h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

handleServerMessage :: Handle -> Server.Message -> IO ()
handleServerMessage h serverMessage = do
  responses <- respond serverMessage
  forM_ responses $ hPutStrLn h . L.unpack . encode
  handleMessages h

respond :: Server.Message -> IO [Client.Message]
respond message = case message of
  Server.Join -> do
    putStrLn "Joined"
    return [Client.Ping]
  Server.GameInit gameInit -> do
    putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
    return [Client.Ping]
  Server.CarPositions carPositions -> do
    return $ [Client.Throttle 0.5]
  Server.Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return [Client.Ping]
